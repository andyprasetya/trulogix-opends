var environments = {};

environments.staging = {
  'httpPort' : 3000,
  'httpsPort' : 3001,
  'envName' : 'staging',
  'hashingSecret' : 'b144700387101111f02422bf234266affcd786eb15289d695b412c0587243947',
	'dsEndPoint' : '-',
	'accessId' : ['0bcd13d4405963f2e1b7c195016b1b393711474815ed188f70f5c7b3c73a261f'],
	'accessKey' : ['cca83f8cc8ca113c45e9511b4d3c01126940d9ab91bf5747cb4c1a44fd5e43c6'],
	'jwtId' : ['e374356a1cb1078679d640fa22133beb094ba42480a56ebe69148c492f0e553b'],
	'accessSignature' : ['462d7a11fb5f6a55eb1493662055ecaad33d4707ec3eaf7fd6069b2491381bee']
};

environments.production = {
  'httpPort' : 5000,
  'httpsPort' : 5001,
  'envName' : 'production',
  'hashingSecret' : 'b144700387101111f02422bf234266affcd786eb15289d695b412c0587243947',
	'dsEndPoint' : '-',
	'accessId' : ['0bcd13d4405963f2e1b7c195016b1b393711474815ed188f70f5c7b3c73a261f'],
	'accessKey' : ['cca83f8cc8ca113c45e9511b4d3c01126940d9ab91bf5747cb4c1a44fd5e43c6'],
	'jwtId' : ['e374356a1cb1078679d640fa22133beb094ba42480a56ebe69148c492f0e553b'],
	'accessSignature' : ['462d7a11fb5f6a55eb1493662055ecaad33d4707ec3eaf7fd6069b2491381bee']
};
var currentEnvironment = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '';
var environmentToExport = typeof(environments[currentEnvironment]) == 'object' ? environments[currentEnvironment] : environments.staging;
module.exports = environmentToExport;